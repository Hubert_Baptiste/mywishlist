<?php

use mywishlist\models\Item;
use mywishlist\models\Utilisateur;
use mywishlist\Controlers\Controleur;
use mywishlist\Controlers\Authenticate;
use mywishlist\Controlers\ControleurListe;
use Illuminate\Database\Capsule\Manager as DB;
require "vendor/autoload.php";


session_start();

$db = new DB();
$db->addConnection(parse_ini_file("src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();
//$_SESSION['idutil'];

$app = new \Slim\Slim([
	'templates.path' => 'src/templates'
]);

$app->get('/', function(){
	$controleur = new Controleur();
	$controleur -> afficherVueGenerale();
});

$app->get('/liste', function(){
	$controleur = new Controleur();
	$controleur ->afficherListe();
});

$app->get('/liste/:liste', function($liste){
	echo "get:$liste";
});

$app->get('/item', function(){
	$controleur = new Controleur();
	$controleur ->afficherItem();
});

$app->get('/item/:item', function($item){
	$controleur = new Controleur();
	$controleur ->afficherItemNumero($item);
});

$app->get('/connexion', function(){
	$controleur = new Authenticate();
	$controleur ->afficherConnexion();
});

$app->post('/connexion', function(){
	$controleur = new Authenticate();
	$controleur->authentifierCompte;
});

$app->post('/enregistretest', function(){
	$controleur = new Authenticate();
	$controleur->creerCompte();
});

$app->get('/creerliste', function(){
	$controleur = new ControleurListe();
	$controleur->afficherCreerListe();
});

$app->post('/creerliste', function(){
	$controleur = new ControleurListe();
	$controleur->creerListe();
});

$app->get('/enregistre', function(){
	$controleur = new Authenticate();
	$controleur ->afficherEnregistrement();
});

$app->get('/reservation', function(){
	$controleur = new Controleur();
	$controleur ->afficherReservation();

});

$app->post('/reservationitem', function($list,$item){
	$controleur = new Controleur();
	$controleur ->reserveritem($list,$item);
});

$app->render('footer.php');
$app->run();

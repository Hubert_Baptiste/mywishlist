<?php

namespace mywishlist\Controlers;
use mywishlist\vue\VueConnexion;
use mywishlist\vue\VueGenerale;
use mywishlist\vue\VueListe;
use mywishlist\vue\VueItem;
use mywishlist\vue\VueEnregistrement;
use mywishlist\vue\Enregistrement;
use mywishlist\models\Liste as Liste;
use mywishlist\vue\VueCreerListe as VueCreerListe;
use mywishlist\models\Item as Item;

class ControleurListe{

	/**
	* Fonction pour créer une liste
	*/
	public function creerListe(){
  $newliste = new Liste();
  $newliste->titre=$_POST['titre'];
  $newliste->description=$_POST['description'];
  $newliste->expiration=$_POST['expiration'];
  $v = new VueCreerListe();
	$newliste->save();
	$v->afficheliste();
	//$liste = Liste::where("user_id","=",$_SESSION['user_id'])->get()->toArray();
  }
	
	
	/**
	* Fonction pour ajouter un item dans une liste
	* @param idliste	
	*			on recherche la liste précise
	*/
	public function ajouterItem($idliste){
    $liste = Liste::find($idliste);

    if($liste->user_id == $_SESSION['user_id']){
      $item = new Item();
      $item->liste_id = $idliste;
      $item->nom = $_POST['nom'];
      $item->descr = $_POST['description'];
      $item->tarif = $_POST['prix'];
      $item->save();
    }

    $liste = Liste::find($idliste)->toArray();

  }


 /**
 * Fonction pour affficher une page après une création de liste
 *
 */
public function afficherCreerListe(){
		$v = new VueCreerListe();
		$v->affiche();
	}





}

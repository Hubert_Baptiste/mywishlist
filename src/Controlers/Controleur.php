<?php

namespace mywishlist\Controlers;

use mywishlist\vue\VueConnexion;
use mywishlist\vue\VueGenerale;
use mywishlist\vue\VueListe;
use mywishlist\vue\VueItem;
use mywishlist\vue\VueEnregistrement;
use mywishlist\vue\Enregistrement;
use mywishlist\models\Item as Item;

class Controleur{





	public function afficherVueGenerale(){
		$v = new VueGenerale();
		$v->afficheDeco(0);
		/*if (isset($_SESSION["idutil"])){
			$val = $_SESSION["idutil"];
			if ( $val == 1){
				$v->affiche(1);
			} else {

			}
		} */
	}

	/**
	*  Fonction pour afficher une liste 
	*/ 
	public function afficherListe(){
		$v = new VueListe();
		$v->afficherListe();
	}

	
	/**
	* Fonction pour afficher une reservation
	*/
	public function afficherReservation(){
		$v = new VueItem();
		$v->afficherReservation();
	}


	/**
	* Fonction pour afficher les items
	*/
	public function afficherItem(){
		$v = new VueItem();
		$v->afficherItem();
	}
	
	/**
	* Fonction pour afficher un item via son numero
	*/
	public function afficherItemNumero($numero){
		$v = new VueItem();
		$v->afficherItemNumero($numero);
	}


	/**
	* Fonction pour ajouter une liste
	*/
	public function ajouterListe($t,$desc,$exp){
		$q1 = new Liste();
		$q1->titre=$t;
		$q1->description=$desc;
		$q1->expiration=$exp;
		$q1->save();
	}


	/**
	* Fonction pour reserver un item
	*/ 
 public function reserverItem($list, $item){
		$app = \Slim\Slim::getInstance();
        $item = Item::where('id', '=', "$item")->get();

        $reserver = filter_var($_POST['reserver'], FILTER_SANITIZE_STRING);
        $message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);

        if (isset($_POST['reserver']) and !empty($reserver)){
            if (isset($_POST['message']) and !empty($message)){
                $item->reserver = $reserver;
                $item->message = $message;
                $_SESSION['pseudoReservation'] = $reserver;
            }
        }

        $item->reserver = 1;
        $item->save();
        $_SESSION['message_reussi'] = "Votre item a été réservé";
				$app->redirect($app->urlFor('item', ['list' => $list, 'item' => $item]));
    }


}

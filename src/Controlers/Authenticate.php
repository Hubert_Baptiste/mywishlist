<?php

namespace mywishlist\Controlers;


use mywishlist\vue\VueConnexion;
use mywishlist\vue\VueGenerale;
use mywishlist\vue\VueEnregistrement;
use mywishlist\vue\VueEnregistrementtest;
use mywishlist\models\Compte as Compte;

class Authenticate{

	/**
	* Fonction pour créer un compte
	*/
	public function creerCompte(){
	$v = new VueEnregistrementtest;
	$erreur =0; //Lorsque le code d'erreur est a 0, cela veut dire que la création de compte s'est déroulée sans soucis
    $login=$_POST['pseudo'];
    $mdp=$_POST['password'];
	$mdpverif=$_POST['password2'];
	$utilisateur = new Compte();
	if (!empty($login) AND !empty($mdp) AND !empty($mdpverif)){ //Vérification que les champs ne sont pas vides
		if ($mdp == $mdpverif){
			$hash=password_hash($mdp, PASSWORD_DEFAULT, ['cost'=> 12] );
			if (!isset($_SESSION['connecte'])) {
			$_SESSION['connecte'] = 0;
			} else {
			$_SESSION['connecte']=1;
			}
			$utilisateur->login=$login;
			$utilisateur->mdp=$hash;
			$utilisateur->save();
			$v ->afficheSucces(); // la connexion est possible, on affiche une page de succes

		} else {
		$v->afficheErreurmdp2(); // les deux mots de passes rentrés ne sont pas les mêmes, on affiche une erreur en conséquence
		}
	} else {
		$v ->afficheErreurVide(); //un ou des champs sont vides, affichage d'une erreur en rapport
	}	return $erreur;


  }
	
	/**
	* Fonction appelé lors de la connexion ( ne fonctionne pas car problème d'utilisation de sessions )
	*/
	public function authentifierCompte(){
	$v = new VueConnexion();
	$login=$_POST['pseudo'];
    $utilisateur = Compte::where("login","=",$login)->first();
    $mdp=$_POST['password'];
    if (password_verify($mdp, $utilisateur->mdp)){
		//$_SESSION["idutil"]=1;
		$v->afficheConnec();
		} else {
		$v->erreurDeConnec();
		}
    }
	
	
	public function afficherConnexion(){
		$v = new VueConnexion();
		$v->affiche();
	}
	/**
	* Une fonction qui est censé être appellé lors de la déconnexion d'un utilisateur.
	*/ 
	public function deconnecte(){
		session_unset();
		session_destroy();
	}

	/**
	* Une fonction qui affiche la page pour s'enregistrer
	*/
	public function afficherEnregistrement(){
		$v = new VueEnregistrement();
		$v->affiche();
	}

}

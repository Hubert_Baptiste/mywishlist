<?php

namespace mywishlist\vue;
use mywishlist\models\Liste as Liste;

class VueListe{

//affichage classique de la vue liste
private static $body = <<<END
<meta charset="utf-8">
<h1>Affichage des listes de souhait</h1>
<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
<body>
  <p> Sur cette page vous pourrez voir toutes les listes de souhait de MyWishList  </p>
  <form method="post" action="./item">
    <button type="submit">Ajouter un item à une liste. Numero de la liste</button>
    <label> <input type="text" name="user_id"/></label><br/>
    </form>
</body>
END;

//affichage de toutes les listes
	public function afficherListe(){
		echo self::$body;
		$table = Liste::all();
		foreach($table as $liste){
		echo $liste->no.' : pour utilisateur numéro'.$liste->user_id.', titre :'.$liste->titre.' décrit tel que '.$liste->descr.'<br>';
  }
  echo ' <a href="./creerliste" class="bouton">Creer une liste</a>';
}
}

?>

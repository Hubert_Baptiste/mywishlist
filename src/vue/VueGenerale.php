<?php

namespace mywishlist\vue;

class VueGenerale{

//affichage lorsque l'on est pas connecté
	public function afficheDeco(){
		$body = <<<END
<!DOCTYPE html>
<html>
	<head>
        <title>MyWishList</title>
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
		<meta charset="utf-8"/>
	</head>
	<h1>MyWishList</h1>
	<div class="intro">
		<p>Êtes-vous déjà membre ?</p>
		<form action="./enregistretest" method="POST" >
		<div align="center">
			<table>
				<tr>
					<td align="right">
						<div class="id">
							<label for="pseudo">Votre pseudo :</label>
							<input type="text" name="pseudo" id="pseudo" placeholder= "Jacques"/>
						</div>
					</td>
				<tr>
					<td align="right">
						<div class="mdp">
							<label for="pass">Votre mot de passe :</label>
							<input type="password" name="pass" id="pass" placeholder="••••••"/>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<a href="connexion" class="bouton">Se Connecter</a>
		<form action="connexion" method="POST" >
	</div>
	<div class="tout">
		<div class="texte">
			<p>Rejoignez-nous :</p>
		</div>
		<div class="compte">
			<a href="enregistre" class="bouton">Créer un compte</a>
			<a href="liste" class="bouton">Afficher Les listes</a>
			<a href="item" class="bouton">Afficher les items</a>
		</div>
	</div>
</html>
END;
			echo $body;
	}

//affichage lorsque l'on est connecté
	public function afficheCo(){
	$body = <<<END
	<!DOCTYPE html>
	<html>
	<head>
        <center><title>MyWishList</title></center>
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
		<meta charset="utf-8"/>
	</head>

	<h1><u>MyWishList</u></h1>

	<div class = "intro">
		<p>MyWishList.app est une application en ligne pour créer, partager et gérer des listes de cadeaux.
		L'application permet à un utilisateur de créer une liste de souhaits à l'occasion d'un événement
		particulier (anniversaire, fin d'année, mariage, retraite …) et lui permet de diffuser cette liste de
		souhaits à un ensemble de personnes concernées. Ces personnes peuvent alors consulter cette liste
		et s'engager à offrir 1 élément de la liste. Cet élément est alors marqué comme réservé dans cette
		liste.</p>
	</div>
	<p>
	<a href="/monCompte" class="bouton"> Accéder à votre compte</a>
	<a href="/" class="bouton"> Déconnexion</a>
	</p>
	</p>
	</form>
	<a href="liste" class="bouton"> Acceder aux listes</a>
	<a href="item" class="bouton"> Acceder aux items</a>
END;
			echo $body;
	}

}

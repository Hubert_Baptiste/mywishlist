<?php

namespace mywishlist\vue;
use mywishlist\models\Item;
use mywishlist\vue\Post;

class VueItem{

//affichage classique de la vue item
private static $body = <<<END
<meta charset="utf-8">
<h1>Affichage des listes de souhait</h1>
<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
<body>
	<p> Sur cette page vous pourrez voir tous les items de MyWishList  </p>
</body>
END;

//affichage de tout les items
	public function afficherItem(){
		echo self::$body;
		$tables = Item::all();
	foreach($tables as $item){
		echo 'id de liste : '.$item->liste_id.', nom item :'.$item->nom.', est '.$item->descr.'   <img src="./img/'.$item->img.'"><br>';
		if($item->reserver==''){
			echo '<form method="post" action="./reservationitem">
			  <label>Nom: <input type="text" name="reserver"/></label>
			  <label>Message: <input type="text" name="message"/></label>
				<button type="submit">Réserver item</button>
			</form>';
		}
	}
}

//affichage de tout les items en fonction du numéro
	public function afficherItemNumero(int $numero){
		echo self::$body;
		$tables = Item::all();

	foreach($tables as $item){
		if($item->liste_id==$numero){
			$app = \Slim\Slim::getInstance();
			$requete = $app->getInstance();
			$path = $requete->getRootUri();
			$r_img = $path."/src/models/img/".$item->img;
			echo 'id de liste : '.$item->liste_id.', nom item :'.$item->nom.', est '.$item->descr.'   <img src="$r_img"><br>';
		}
	}

}

}
?>

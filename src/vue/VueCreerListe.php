<?php

namespace mywishlist\vue;
use mywishlist\models\Liste as Liste;

class VueCreerListe{
//affichage classique de la création d'une liste
private static $body = <<<END
<!DOCTYPE html>
<html>
	<head>
        <title>MyWishList</title>
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
		<meta charset="utf-8"/>
	</head><h1>Creer une liste</h1>
<body>

<p> Sur cette page, vous pourrez creer votre listes de souhait de MyWishList  </p>
<form method="post" action="./creerliste">
  <label>titre: <input type="text" name="titre"/></label><br/>
  <label>description: <input type="text" name="description"/></label><br/>
  <label>expiration: <input type="text" name="expiration"/></label><br/>
	<button type="submit">Créer une liste</button>
</form>
</body>
</html>
END;

////affichage de la création d'une liste lorsque ça fonctionne
private static $body2 = <<<END
<!DOCTYPE html>
<html>
	<head>
        <title>MyWishList</title>
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
		<meta charset="utf-8"/>
	</head><h1>Creer une liste</h1>
<body>

<p> Vous avez bien créé la liste </p>
<a href="./" class="bouton">Retour à l'accueuil </a>
</body>
</html>
END;

//choix de l'affichage

public static function affiche(){
  echo self::$body;
}

public static function afficheliste(){
  echo self::$body2;
}

}
?>

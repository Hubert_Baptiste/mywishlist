<?php

namespace mywishlist\vue;

class VueEnregistrementtest{

	//affichage lorsque la connexion est réussie
	public function afficheSucces(){
		$body = <<<END

		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?>" media="all" />
		<h1>Inscription à la base de donnée MyWishList</h1>'
		<div align="center">
			<p>connexion réussie </p>
		</div>
		</body>
END;
		echo $body;
	}

//affichage lorsqu'il y a une erreur de mot de passe
	public function afficheErreurmdp2(){
		$body =<<<END

		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?>" media="all" />
		<h1>Inscription à la base de donnée MyWishList</h1>'
		<div align="center">
			<p> Connexion échouée, vous n'avez pas rentré les deux mêmes mots de passes </p>
		</div>

		<div class="intro1">
		<a href="./enregistre" class="bouton">Reesayer l'enregistrement</a>
		</div>
		</body>
END;
		echo $body;
	}

	//affichage lorsqu'il n'y a pas de champs rempli
	public function afficheErreurVide(){
		$body =<<<END
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?>" media="all" />
		<h1>Inscription à la base de donnée MyWishList</h1>'
		<div align="center">
			<p> Il y a un champ ou plusieurs non rempli(s)</p>
		</div>
		<div class="intro1">
		<a href="./enregistre" class="bouton">Reesayer l'enregistrement</a>
		</div>
		</body>
END;
		echo $body;
	}
}

?>

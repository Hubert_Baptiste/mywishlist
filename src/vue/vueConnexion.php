<?php
namespace mywishlist\vue;

class VueConnexion{
//affichage de la vue connexion
	public static function affiche(){
		$body ="";
		$body =<<<END
	<!DOCTYPE html>
	<html>
	<head>
        <title>MyWishList</title>
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
		<meta charset="utf-8"/>
	</head>
	<h1>Mon compte</h1>
	<div class="intro1">
		<a href="./liste" class="bouton1">Accéder aux listes</a>
		<a href="./item" class="bouton1">Accéder aux items</a>
	</div>
	<div class="tout">
		<div class="compte">
			<a href="deconnexion" class="bouton">Se déconnecter</a>
		</div>
	</div>
</html>
END;
		echo $body;
	}
	
	/**
	* Affichage dans le cas d'une connexion fructueuse
	*/
	public static function afficheConnec(){
		$body =<<<END
	<!DOCTYPE html>
	<html>
	<head>
        <title>MyWishList</title>
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
		<meta charset="utf-8"/>
	</head>
	<h1>Vous êtes connecté</h1>
	<div class="tout">
		<div class="compte">
			<a href="./" class="bouton">Reessayer</a>
		</div>
	</div>
</html>
END;
	echo $body;
	}
	
	/**
	* Affichage dans le cas d'une connexion infructueuse
	*/
	public static function erreurDeConnec(){
		$body =<<<END
	<!DOCTYPE html>
	<html>
	<head>
        <title>MyWishList</title>
		<link rel="stylesheet" type="text/css" href= "web/VueGenerale.css?<? echo time(); ?" />
		<meta charset="utf-8"/>
	</head>
	<h1>Vous êtes connecté</h1>
	<div class="tout">
		<div class="compte">
			<a href="./" class="bouton">Reessayer</a>
		</div>
	</div>
</html>
END;
	echo $body;
	}
}
